<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'no_ktp' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'foto_ktp' => 'required|max:10000|mimes:jpg,jpeg,png',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Semua data wajib diisi!',
                'data'   => $validator->errors()
            ], 401);
        }

        $randName = Str::random(34);
        $request->file('foto_ktp')->move(storage_path('foto_ktp'), $randName);

        $post = User::create([
            'nama'   => $request->input('nama'),
            'tempat_lahir' => $request->input('tempat_lahir'),
            'tanggal_lahir' => $request->input('tanggal_lahir'),
            'no_ktp' => $request->input('no_ktp'),
            'jenis_kelamin' => $request->input('jenis_kelamin'),
            'alamat' => $request->input('alamat'),
            'foto_ktp' => storage_path('foto_ktp'),
            'password' => password_hash($request->input('password'), PASSWORD_BCRYPT),
        ]);

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Registrasi berhasil!',
                'data' => $post
            ], 201);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Registrasi Gagal!',
            ], 400);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'   => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Semua data wajib diisi!',
                'data'   => $validator->errors()
            ], 401);
        }

        $username = $request->input('username');
        $password = $request->input('password');

        $user = User::where('no_ktp', $username)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Akun tidak ditemukan!',
            ], 401);
        }

        $isValidPassword = Hash::check($password, $user->password);
        if (!$isValidPassword) {
            return response()->json([
                'success' => false,
                'message' => 'Username / password salah!',
            ], 401);
        }

        $generateToken = bin2hex(random_bytes(40));
        $user->token = $generateToken;
        $user->update();

        return response()->json([
            'success' => true,
            'message' => 'Login berhasil!',
            'data' => $user
        ], 200);
    }
}
