<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_foto($name)
    {
        $avatar_path = storage_path('foto_ktp') . '/' . $name;
        if (file_exists($avatar_path)) {
            $file = file_get_contents($avatar_path);
            return response($file, 200)->header('Content-Type', 'image/jpeg');
        }
        $res['success'] = false;
        $res['message'] = "Avatar not found";

        return $res;
    }

    public function view(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'   => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Semua data wajib diisi!',
                'data'   => $validator->errors()
            ], 401);
        }

        $id = $request->input('id');

        $user = User::where('id', $id)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Akun tidak ditemukan!',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data ditemukan!',
            'data' => $user
        ], 200);
    }
}
